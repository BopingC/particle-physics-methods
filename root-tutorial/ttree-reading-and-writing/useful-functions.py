import numpy as np
import ROOT
from typing import Optional, List

def branch_to_array(tree: ROOT.TTree,
                    branch_name: str,
                    n_events: Optional[int] = None) -> np.ndarray:
    """!
    @brief Takes ROOT tree and the name of the leaf and returns it as a numpy array

    @param[in] tree: ROOT.TTree object
    @param[in] branch_name: The name of the leaf(branch) you want as a string
    @param[in] n_events: Number of events to use. If not set, defaults to as many as available

    @returns Numpy array of branch
    """
    # each time we'll check to make sure that the event we're on is less than this
    if isinstance(n_events, int):
        maximum_event_number = n_events
    else:
        maximum_event_number = np.inf

    try:  # first try it the fast way
        out = np.array(tree.AsMatrix([branch_name])).flatten()
        if isinstance(maximum_event_number, int):
            out = out[:maximum_event_number]
        return out

    # if that doesn't work due to branches having different names as leaves, try it the slow way
    except:
        out = []
        current_event_number = 0
        for name in get_branch_names(tree):
            if name == branch_name:
                for event in tree:
                    if current_event_number == maximum_event_number:
                        break
                    else:
                        out.append(eval('event.' + branch_name))
                        current_event_number += 1
                return np.array(out)
        else:
            raise ValueError('Branch named "%s" not found in tree %s.' % (branch_name, tree.GetName()))


def get_branch_names(tree: ROOT.TTree) -> List[str]:
    """!
    @brief Get names of branches
    """
    out = []
    for branch in tree.GetListOfBranches():
        out.append(branch.GetName())
    return out
