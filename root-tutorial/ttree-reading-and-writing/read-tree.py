import ROOT
from array import array


# get file
f = ROOT.TFile("my_file.root", "READ")

# fetch the tree
t = f.Get("ntuple")

# setup variables
pT  = array('f', [0.0])
eta = array('f', [0.0])
phi = array('f', [0.0])

# tie them to branches
t.SetBranchAddress("pT",  pT)
t.SetBranchAddress("eta", eta)
t.SetBranchAddress("phi", phi)

# read events
average_pT  = 0
average_eta = 0
average_phi = 0

for i in range(t.GetEntries()):
    t.GetEntry(i)
    average_pT  += pT[0]
    average_eta += eta[0]
    average_phi += phi[0]

average_pT  /= t.GetEntries()
average_eta /= t.GetEntries()
average_phi /= t.GetEntries()

f.Close()

print('Average pT:  ', average_pT )
print('Average eta: ', average_eta)
print('Average phi: ', average_phi)
