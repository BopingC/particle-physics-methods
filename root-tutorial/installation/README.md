# Installing ROOT

## On local machine
There are two ways to install ROOT on your local machine. You can either download the precompiled binaries (recommended for most people) or compile yourself from the source code (only if you need control over certain options).

Instructions for both can be found on ROOT's website.

### Downloading precompiled binaries
Taken from [here](https://root.cern/install/#download-a-pre-compiled-binary-distribution).
Downloading on a Centos8 machine: (pick the one corresponding to your machine)
```bash
wget https://root.cern/download/root_v6.26.10.Linux-centos8-x86_64-gcc8.5.tar.gz
tar -xzvf root_v6.26.10.Linux-centos8-x86_64-gcc8.5.tar.gz
source root/bin/thisroot.sh # also available: thisroot.{csh,fish,bat}
```
**Note**: Add the *source root/bin/thisroot.sh* part to your *~/.bashrc* file, so you don't need to do it every time you open a new terminal.

### Compiling from source
Do this at your own risk. Takes a while and can be annoying. Instructions are here:
[https://root.cern/install/#build-from-source](https://root.cern/install/#build-from-source)

## Via package manager
This instructions also come from [ROOT's website](https://root.cern/install/#linux-package-managers) where the most up-to-date info can be found

### Snap
On most Linux distros, Snap can be used to install ROOT
```bash
sudo snap install root-framework
snap run root-framework
# or if there is no fear of conflicts with other installations:
root
```

### Other package managers
Depending on the distro you have, there may be another package manager that works. Check [ROOT's website](https://root.cern/install/#linux-package-managers) for your specific machine


## Conda (Recommended for beginners)
You can start an environment in Conda with ROOT baked in.
Start by [downloading Anaconda or Conda](https://www.anaconda.com/products/distribution).

You can start a ROOT environment as follows:
```bash
conda config --set channel_priority strict
conda create -c conda-forge --name root-environment root
conda activate root-environment
```
where you can name it anything you want, I've just chosen *root-environment* here.
Then anytime you want to use it, just run
```bash
conda activate root-environment
```
within conda.

## Docker
If you like using docker containers, you can run ROOT using the official Docker image like so:
```bash
docker run -it rootproject/root
```

## lxplus
If you're affiliated with CERN or another insitute that uses lxplus, you should have ROOT already built into lxplus. Note that to see graphics, you'll want to do
```bash
ssh -XY username@lxplus.cern.ch
```
rather than the normal
```bash
ssh username@lxplus.cern.ch
```
