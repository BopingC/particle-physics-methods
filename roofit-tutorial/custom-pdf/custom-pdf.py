import ROOT
import numpy as np
from scipy.stats import rv_continuous
from tqdm import tqdm
from matplotlib import pyplot as plt

#--------------------------------
# define useful function
#--------------------------------
def normalize(hist):
    integral = 0.0
    for i in range(hist.GetNbinsX()+1):
        integral += hist.GetBinContent(i) * hist.GetBinWidth(i)
    hist.Scale(1.0 / integral)
    return hist


# -------------------------------
# setup pathological function
# -------------------------------
class pathological_pdf(rv_continuous):
    def _pdf(self, x, a, b):
        return (-a*(x**2) + b) / 10.66667

#--------------------------------
# produce data
# -------------------------------

# true vals
a_true = 2.0
b_true = 8

n_points = 10000
x_min = 0
x_max = 2.0

# create data
my_pdf = pathological_pdf(a=x_min, b=x_max,
                          shapes='a,b')
print("Creating data...")
vals = my_pdf.rvs(a_true,
                  b_true,
                  size=n_points)
print("Data created")


#---------------------------------
# set up RooFit model
# --------------------------------
x_roo = ROOT.RooRealVar("x","x", x_min, max(vals))
a_roo = ROOT.RooRealVar("a", "a", 1, 0, 10)
b_roo = ROOT.RooRealVar("b", "b", 1, 0, 10)


roo_pdf = ROOT.RooGenericPdf("roo_pdf",
                             "(-a*pow(x,2) + b)", [x_roo, a_roo, b_roo])

#----------------------------------
# fit to data
#----------------------------------

# make a RooDataSet from the random gaussian set from earlier
data = ROOT.RooDataSet.from_numpy({'x':vals}, [x_roo])

# fit to data
roo_pdf.fitTo(data)

# print fit parameters
print(a_roo)
print(b_roo)


#----------------------------------
# plot
#----------------------------------
# create data histogram
hist = ROOT.TH1F("hist", "hist", 30, 0, x_max)
for val in vals:
    hist.Fill(val)
hist = normalize(hist)
hist.SetStats(0)

# create fit histogram
fit_hist = ROOT.TH1F("fit", "fit", 100, 0, x_max)
for i in range(100):
    x = fit_hist.GetBinCenter(i)
    val = (-1 * a_roo.getVal() * x**2) + b_roo.getVal()

    fit_hist.SetBinContent(i, val)
fit_hist = normalize(fit_hist)

hist.SetLineWidth(3)
fit_hist.SetLineWidth(3)
fit_hist.SetFillStyle(0)
fit_hist.SetLineColor(ROOT.kRed-6)

leg = ROOT.TLegend()
leg.AddEntry(hist, 'Data', 'lep')
leg.AddEntry(fit_hist, 'Fit', 'l')

# plot
canvas = ROOT.TCanvas()
hist.Draw()
fit_hist.Draw("hist, c, same")
leg.Draw('same')
canvas.Update()
canvas.SaveAs("custom-function.png")
