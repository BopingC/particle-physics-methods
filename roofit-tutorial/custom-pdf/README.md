# Custom Fit Function
RooFit has a lot of PDFs baked into it. Like, a lot. Probably all that you would ever need. However, let's say you need to do a fit to a PDF that isn't included. The good news is that you can write your own!

Let's say we need to do a fit of the form
```math
p(x) = -ax^2 + b
```

We can do this with a *RooGenericPdf*, in this case
```python
x_roo = ROOT.RooRealVar("x", "x", x_min, max(vals))
a_roo = ROOT.RooRealVar("a", "a", 1, 0, 10)
b_roo = ROOT.RooRealVar("b", "b", 1, 0, 10)
ROOT.RooGenericPDF("roo_pdf", # title
		   "(-a*pow(x,2)+b)", [x_roo, a_roo, b_roo])
```

Then you can simply fit to data as with any other PDF!

![plot](custom-function.png)


# Homework
First, run [generate-homework-data.py](generate-homework-data.py) to generate a 1D dataset.

Now, we know its distribution is of the form
```math
P(x) = -a(x-c)^3 + b
```

Our job is to find *a*, *b*, and *c*. I'll give you a hint. We know that:
```math
0 < a < 1
```
```math
0 < b < 1
```
```math
0 < c < 5
```

When all is said and done, your distribution with fit should look something like this:

![plot](homework-plot.png)
