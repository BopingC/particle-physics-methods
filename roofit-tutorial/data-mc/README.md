# Data/MC Fit

Let's say you have three Monte Carlo (MC) samples and some data. The MC should fit the data, but when you stack the MC, it doesn't quite fit.

![mc data](data-mc-pre.png)

We want to come up with some correction factors: $`c_1, c_2, c_3`$ to adjust the MC by to make it fit the data.

It should look like
```math
(c_1 * \text{MC}_1) + (c_2 * \text{MC}_2) + (c_3 * \text{MC}_3) = \text{data}
```

If you recall from the [bump-hunt lesson](../bump-hunt), we can use the *RooAddPdf* command to combine PDFs.
However, we don't have a PDF here, we have a sample. Luckily, we can set up a *RooHistPdf* object.

Therefore, we start by creating *RooDataHist* objects for the *TH1F* histograms for all the MC and data.

```python
pT = ROOT.RooRealVar('pT', 'pT', 0, 1000)
mc1_roo_hist = ROOT.RooDataHist('mc1_roohist', 'mc1_roohist', pT, mc_1_hist)
mc2_roo_hist = ROOT.RooDataHist('mc2_roohist', 'mc2_roohist', pT, mc_2_hist)
mc3_roo_hist = ROOT.RooDataHist('mc3_roohist', 'mc3_roohist', pT, mc_3_hist)

data_roo_hist = ROOT.RooDataHist('data_roohist', 'data_roohist', pT, data_hist)

```

These can be converted to PDFs easily:
```python
# make hist pdfs
mc1_roo_pdf = ROOT.RooHistPdf("mc1_pdf", "mc1_pdf", pT, mc1_roo_hist)
mc2_roo_pdf = ROOT.RooHistPdf("mc2_pdf", "mc2_pdf", pT, mc2_roo_hist)
mc3_roo_pdf = ROOT.RooHistPdf("mc3_pdf", "mc3_pdf", pT, mc3_roo_hist)
```

Then, we can initialize the constants that we need to find:
```python
# set constants c1, c2, c3 so that
# (c1*mc1) +  (c2*mc2) +  (c3*mc3) = data
c1 = ROOT.RooRealVar('c1', 'c1', 1, 0, 1e10)
c2 = ROOT.RooRealVar('c2', 'c2', 1, 0, 1e10)
c3 = ROOT.RooRealVar('c3', 'c3', 1, 0, 1e10)
```

Finally, the MC PDFs can be combined wiht *RooAddPdf* and fit to the data histogram:
```python
model = ROOT.RooAddPdf("model", "model",
                       ROOT.RooArgList(mc1_roo_pdf,
                                       mc2_roo_pdf,
                                       mc3_roo_pdf),
                       ROOT.RooArgList(c1, c2, c3))
model.fitTo(data_roo_hist)
```

After doing this and scaling to account for the normalization: we know our constants and the fit looks great!

![mc data](data-mc-post.png)


# Homework
You know the drill by now. First, run [generate-homework-dataset.py](generate-homework-dataset.py)

This generates the following dataset of 2 MC and data:

![homework pre](homework-pre.png)


Then find *c1* and *c2* to fit the MC to the data.

When all is said and done, your results should look like this:

![homework post](homework-post.png)
