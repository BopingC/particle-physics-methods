# Gaussian Fit

This tutorial does a very simple fit of a Gaussian model to a Gaussian histogram. RooFit's job is to find the mean and standard deviation. The blue points denote the data, and the black line denotes the fit.

The script also prints out the true mean and standard deviation alongside the RooFit fit estimate of these values.

![plot](gaussian.png)

First, Numpy is used to create some random gaussian data with $`\mu = 3.2`$ and $`\sigma = 1.6`$
```python
mean = 3.2
sigma = 1.3
gaussian_data = np.random.normal(loc=mean, scale=sigma, size = 10000)
```

For the sake of the rest of this project, let's assume we don't know the truth values of mean and sigma. What we want to do is see if we can just take this distribution and find out what they are using RooFit!

To do this, we have to set up our RooFit model. In RooFit, variables used are of the RooVar type. In this case, we have real numbers we're modeling, so we'll use RooRealVar.

Here, lets let x_var be the variable with a gaussian distribution being modeled. We'll assume it's bounded between $`-20 \rightarrow 20`$.
```python
x_var = ROOT.RooRealVar("x", "x", -20, 20)
```

We know we have a Gaussian distribution, and we want to find the values of `$\mu`$ and $`\sigma`$. So, let's define a variable for each of those (guessing their initial values) and create a Gaussian distribution.

Our initial guess will be $`\mu = 0`$, possible range $`-20 \rightarrow 20`$ and $`\sigma=1`$, possible range $`0 \rightarrow 10`$. 

```python
mean_var = ROOT.RooRealVar("mean", "mean of gaussian", 0, -20, 20)
sigma_var = ROOT.RooRealVar("sigma", "std of gaussian", 1, 0, 10)

# create gaussian PDF using these variables
gauss = ROOT.RooGaussian("gauss", "gaussianPDF", x_var, mean_var, sigma_var)
```

Now for the fun part! We've created our model, and now we can fit it to the data.
```python
# assign x_var (the RooFit variable) to the gaussian data we made with numpy
data = ROOT.RooDataSet.from_numpy({'x':gaussian_data}, [x_var])

# fit our model to the data
gauss.fitTo(data)

# find out what mean and standard deviation it found from the fit
roofit_mean = gauss.getMean().getVal()
roofit_sigma = gauss.getSigma().getVal()
```


Printing the results shows that RooFit does a nice job!
```
True mean: 3.2    RooFit estimated mean: 3.162398087736843
True sigma: 1.3   RooFit estimated sigma: 1.2923821053482025
```


# Homework
We've seen how this can be done with a Gaussian function, now let's try something a bit more complicated. A [Crystal Ball distribution](https://en.wikipedia.org/wiki/Crystal_Ball_function) is used sometimes. It has six variables to fit: $`m_0`$, $`\sigma`$, $`\alpha_L`$, $`\eta_L`$, $`\alpha_R`$, and $`\eta_R`$.
[This](https://root.cern.ch/doc/master/classRooCrystalBall.html#ae323c7f61647b4fb193d552bf393083d) is the corresponding RooFit distribution (the third constructor is the one we want).

First, run [generate-homework-data.py](generate-homework-data.py) to generate the dataset. Next, see if you can find the values of the fit variables that I used when generating the distribution. The procedure will be very similar to what I did in the Gaussian example.

## Solutions
The solution script is [homework-solutions.py](homework-solutions.py)
If you're feeling creative, try and make a plot. It should look something like this:
![plot](crystal-ball.png)
