# Bump Hunt

Let's use RooFit to find a new particle! We know a few things:
- The particle's mass is somewhere between 60 --> 140 GeV
- The background follows a gamma distribution
- We have 100,000 events of data


We can do what is known as a bump hunt, in which we model both signal and background together.
We want to find out a few things in this bump hunt:
- What is the particle's mass?
- How prevalent is it in our dataset?


This time we have to declare two distributions: one for the background (gamma) and one for signal (Gaussian). We'll start by defining our variables

```python
# this is the distribution we'll be looking at
mass = ROOT.RooRealVar("mass", "mass", 0, 200)

# background variables
gamma = ROOT.RooRealVar("gamma", "gamma", 1, 0, 1e6)
beta = ROOT.RooRealVar("beta", "beta",    1, 0, 1e6)
mu = ROOT.RooRealVar("mu", "mu",          0, 0, 0) # fix this at 0

# signal variables
# we want to find a particle in the 60 --> 140 GeV range
mean = ROOT.RooRealVar("mean", "mean",    80, 60, 140)
sigma = ROOT.RooRealVar("sigma", "sigma", 1, 0, 40)
```

This time, we'll add one more variable that, upon being fit, will tell us what fraction of our dataset is signal:
```python
# what fraction is signal
# initial guess = half
fsig = ROOT.RooRealVar("fsig", "fsig", 0.5, 0, 1)
```

Now we'll make our two distributions
```python
# background
sig = ROOT.RooGaussian("sig", "sig",
                       mass, mean, sigma)
# background
bkg = ROOT.RooGamma("bkg", "bkg",
                    mass, gamma, beta, mu)
```

Since the dataset will be some combination of:
```math
model = (fSig * sig) + ((1-fSig) * bkg)
```

We'll use *RooAddPdf* to combine our distributions like so
```python
# total model is the sum of those
model = ROOT.RooAddPdf("model", "model",
                       ROOT.RooArgList(sig, bkg), fsig)
```

Now we can fit *model* to the data as before, and find out the values.
We learn that the mass of our particle is $`\approx 100`$ GeV from *mean*, and from *fSig* we learn that about 10% of our dataset has these signal events.

Plotting the results yields:

![plot](bump-hunt.png)

# Homework
You are trying to discover the Carlson boson. It has a mass somewhere between 100 --> 150 GeV.

Run [generate-homework-data.py](generate-homework-data.py) to produce the dataset of masses. We know that the particle is somewhere here. Its mass distribution is likely *Gaussian* on top of the *exponential* decay shape of the background distribution.

What is the Carlson boson mass? What fraction of events in this dataset contain the particle?

Your final distribution will look something like this:

![homework-solutions.png](homework-solutions.png)