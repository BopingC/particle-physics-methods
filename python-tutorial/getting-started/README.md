# Getting Started in Python

## Python in the terminal
In the terminal, type *python*

This should pull up something like
```
Python 3.8.10 (default, Mar 15 2022, 12:22:08) 
[GCC 9.4.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> 
```

Now you can enter commands. For instance, type
```python
>>> print("hello world")
```

and you should see
```python
>>> print("hello world")
hello world
```

## Python files
You can also write python in files with a *.py* ending and run them as follows:
<br>
**my_script.py**
```python
print("hello world")
```
**In terminal**
```bash
$ python my_script.py 
hello world
```



## Python as a calculator
Python can do math quite well

```python
>>> 5+2
7
>>> 5.2+4.5
9.7
>>> 4/0.1
40.0
>>> 3-(-5)
8
```

One new operator is the remainder operator. It gives you the remainder from a division
```python
>>> 8 % 3
2
>>> 2 % 3
2
>>> 44 % 5
4
>>> 12.2 % 3
0.1999999999999993
>>> 4.4 % 1.3
0.5000000000000002
```

## Variables
You can save anything as a variable in Python. To do so, just give it a name. Here, I define variables a and b. I then multiply them together.
```python
>>> a = 5.3
>>> b = 12.2
>>> a * b
64.66
```

Anything can be a variable in Python. Integers, strings, etc. You can also redefine variables whenever you want
```python
>>> a = 4.2
>>> print(a)
4.2
>>> a = 'hi there'
>>> print(a)
hi there
```

## Lists and tuples
If you want to store a few values in one variable, you have two options for that: *lists* and *tuples*. Both objects hold a few data points. For instance, if you wanted to store the energies of 4 particles in your experiment, rather than doing
```python
energy_1 = 1.2
energy_2 = 3.4
energy_3 = 2.1
energy_4 = 4.2
```

you can use a *list* like so
```python
particle_energies = [1.2, 3.4, 2.1, 4.2]
```

You can also change lists by adding an element. For instance, now you have to consider a 5th particle of energy 12.4. You can add that to the end of the list like so:
```python
>>> particle_energies.append(12.4)
>>> print(particle_energies)
[1.2, 3.4, 2.1, 4.2, 12.4]
```

To fetch a certain element in a list, you can just use its index like so:
```python
>>> first_energy = particle_energies[0]
>>> print(first_energy)
1.2
>>> fourth_energy = particle_energies[3]
>>> print(fourth_energy)
4.2
```
you'll note that lists in python are zero-indexed, which just means the first element is at index 0, the second is at index 1 and so on

Sometimes you may need to change an object in a list. For instance, let's say you remeasure the energy of particle 3 and discover it to be 9.9, not 2.1.  You can replace that value in the list like so:
```python
>>> print(particle_energies)
[1.2, 3.4, 2.1, 4.2, 12.4]
>>> particle_energies[2] = 9.9
>>> print(particle_energies)
[1.2, 3.4, 9.9, 4.2, 12.4]
```

We saw earlier how to append an element at the end of a list with the .append() command. Sometimes you'll want to add an element somewhere specific. For instance, you measure a new particle of energy 14.3 that you want to be the third place (index 2). If you want to add this element
```python
>>> print(particle_energies)
[1.2, 3.4, 9.9, 4.2, 12.4]
>>> particle_energies.insert(2, 14.3)
>>> print(particle_energies)
[1.2, 3.4, 14.3, 9.9, 4.2, 12.4]
```

If you want to sort your list by the entries you can do
```python
>>> particle_energies.sort()
>>> print(particle_energies)
[1.2, 3.4, 4.2, 9.9, 12.4, 14.3]
```

Note: Python does not care what is in your list. Unlike other languages, you can put whatever you want in there. For instance, let's say I want to make a list with 3 elements: a string, a number, and a list (yes you can put a list in a list). This is possible like so:
```python
myList = ['a string', -3.3, [12.2, 3.4]]
```

### Tuples
A tuple is like a list, except that it is *immutable*. Therefore, it cannot be changed. It is set in stone, so to say. For instance, trying to append something to a tuple throws an error code.
```python
>>> myTuple = (1,2,4)  # round instead of square brackets
>>> myTuple.append(5)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: 'tuple' object has no attribute 'append'
```

## Dictionaries
Dictionaries in Python are a generalized *key-value pair*. They're best explained with an example. Let's say you run a day-care and want to keep track of each child's month of birth. A list may not be very useful here, since it would just be a list of months. With a dictionary you can:
```python
birthday_months = {'Dave': 'October',
                   'Martin': 'June',
		   'Amy': 'January',
		   'Eliza': 'February'}
```
Then to find out Dave's birth month, you can simply call:
```python
>>> birthday_months['Dave']
'October'
```
Dictionaries can be further nested. If, for instance, you wanted to know each student's birth month, vaccination status, and age, you could do:
```python
student_data = {'Dave': {'birth month': 'October',
                         'vaccinated': True,
			 'age': 4},
                'Martin': {'birth month': 'June',
                         'vaccinated': True,
			 'age': 3},
                'Amy': {'birth month': 'January',
                         'vaccinated': False,
			 'age': 5},
                'Eliza': {'birth month': 'February',
                         'vaccinated': True,
			 'age': 4}
		}
```
Then to find out Amy's age you could do:
```python
>>> student_data['Amy']['age']
5
```

# Homework
You run a school with 5 students. Alpha, Beta, Gamma, Delta, and Epsilon. You've been tasked with using Python to more efficiently.
1. In a dictionary, save their letter grades associated with their names (make them up).
2. In a list, save their ages (make this up too).
3. A new student, Omega, has been added to the school roster. Please add her grade to the dictionary and her age to the list.
4. Find the average age of the students.