h_lit = 6.62607015 # ignore the exponent
h_exp = 6.60
h_exp_err = 0.01

sigma = abs(h_lit - h_exp) / h_exp_err
print('Standard deviations away:', sigma)

if sigma < 1:
    print('The error is acceptable')
elif sigma < 2:
    print('The error is probably unacceptable')
else:
    print('The error is definitely unacceptable')
