# Numpy and Scipy

## Numpy

Numpy is a package for scientific computing in Python, mostly using arrays.

Wait, I can already use lists and tuples, why bother? Because, Numpy is written in C on the backend, making it absurdly fast.

### Installing
```bash
pip install numpy
```

### Arrays

A numpy array is created similarly to a Python one. You can add, subtract, and multiply element-wise, which is different from python lists

```python
>>> import numpy as np
>>> arr_1 = np.array([3.3, 2.1, 4.4, 5.7, 8.9, 1.0])
>>> arr_2 = np.array([6.1, 8.5, 1.1, 5.9, 1.2, 5.0])
>>> arr_1 + arr_2
array([ 9.4, 10.6,  5.5, 11.6, 10.1,  6. ])
>>> arr_1 / arr_2
array([0.54098361, 0.24705882, 4.        , 0.96610169, 7.41666667,
       0.2       ])
>>> arr_1 - arr_2
array([-2.8, -6.4,  3.3, -0.2,  7.7, -4. ])
>>> np.sum(arr_1)
25.4
>>> np.sort(arr_2)
array([1.1, 1.2, 5. , 5.9, 6.1, 8.5])
```

### Speed improvement
Numpy is much faster than pure Python for many things. For instance, taking the dot product of large arrays. Comparing the speed for a dot product of 10,000 elements per array, Numpy is 114X faster.

*Script*
```python
import numpy as np
from time import process_time

a = np.linspace(-5,  12,   10000)   # 10000 equally spaced points from -5 --> 12
b = np.linspace(100, 108, 10000) # 10000 more equally spaced points from 100-->108


# pure python implementation
t1 = process_time()
dot_product = 0
for x,y in zip(a,b):
    dot_product += x*y
t2 = process_time()

py_time = t2 - t1
print('Python time: ', py_time)


# numpy implementation
t1 = process_time()
dot_product = np.dot(a,b)
t2 = process_time()

np_time = t2 - t1
print('Numpy time: ', np_time)

print('Numpy speed improvement: ', round(py_time / np_time), 'X faster')
```

*Output*
```
Python time:  0.002037999999999998
Numpy time:  1.7900000000001248e-05
Numpy speed improvement:  114 X faster

```

### Resources
I can't even begin to describe all that Numpy can do. It has a nice random number generator, good interfaces with other frameworks, a great matrix and linear algebra calculator, and more.

All I can say is that, if you find yourself using python lists, you should ask yourself "Does Numpy have an implementation of this?". It's usually worth a Google and can save you lots of time in the future.

### Homework
Following my example above, compare the speed of binning using [numpy.digitize](https://numpy.org/doc/stable/reference/generated/numpy.digitize.html) to a native python binning implementation. Use uniformly distributed data. This can be produced with [numpy.random.uniform](https://numpy.org/doc/stable/reference/random/generated/numpy.random.uniform.html)

## Scipy
Scipy is a great package for scientific computing. It includes lots of tools for linear algebra, equation solving, differential equations, and statistics

### Installing
```bash
pip install scipy
```

### Uses

#### Special Functions

```python
>>> import scipy.special as sc

# bessel function of first kind of real order
>>> sc.jv(3, 1.4)
0.050497713288951286

# binomial distribution CDF
>>> sc.bdtr(12, 25, 0.5)
0.49999999999999983

# gamma function
>>> sc.gamma(12.3)
83385367.89996998

# error function of complex argument
>>> sc.erf(1.1 + 2.3j)
(-12.404626793451621-2.330491973570498j)

# chebyshev polynomial evaluation
>>> sc.eval_chebyt(4, 3.1)
662.9368000000002

# hermite polynomial evaluation
>>> sc.hermite(8)
poly1d([ 2.56000000e+02,  0.00000000e+00, -3.58400000e+03,  4.54747351e-13,
        1.34400000e+04, -1.81898940e-12, -1.34400000e+04,  3.41060513e-13,
        1.68000000e+03])
```

#### Integration

```python
>>> import numpy as np
>>> import scipy.integrate as integrate

>>> def function_to_integrate(x, a, b):
...    return a*np.tanh(np.sin(b-x))

>>> result = integrate.quad(function_to_integrate,
...                        -5.1, 3.3, # lower and upper limits
...                        args=(1.2, 3.4)) # a and b in the function_to_integrate

>>> print(result)
(1.556959479782644, 1.246960827780072e-09)
```

Here, the integral of my weird function I defined between -5.1 and 3.3 is 1.557 +/- 1.24*10^-9


#### Optimization

##### Curve Fitting


##### Root finding
To find the root of x + 2cos(x) = 0, do as follows. Here, the initial guess is 0.3

```python
>>> import numpy as np
>>> from scipy.optimize import root

>>> def func(x):
...    return x + 2 * np.cos(x)

>>> sol = root(func, 0.3)

>>> print(sol.x)
array([-1.02986653])
```

##### Other optimization algorithms
Finding extrema, graph theory solutions, etc.


#### Fourier Transform
```python
>>> from scipy.fft import fft
>>> import numpy as np
>>> x = np.array([1.0, 2.0, 1.0, -1.0, 1.5])
>>> fft(x)
array([ 4.5       -0.j        ,  2.08155948-1.65109876j,
       -1.83155948+1.60822041j, -1.83155948-1.60822041j,
        2.08155948+1.65109876j])
```


#### Linear Algebra

##### Finding inverse of matrix
```python
>>> import numpy as np
>>> from scipy import linalg
A = np.array([[1,3,5],[2,5,1],[2,3,8]])
>>> print(A)
array([[1, 3, 5],
      [2, 5, 1],
      [2, 3, 8]])
>>> linalg.inv(A)
array([[-1.48,  0.36,  0.88],
      [ 0.56,  0.08, -0.36],
      [ 0.16, -0.12,  0.04]])
```

##### Solving a system of linear equations
Solve
```math
x + 3y + 5z = 10
```

```math
2x + 5y + z = 8
```

```math
2x + 3y + 8z = 3
```

```python
>>> import numpy as np
>>> from scipy import linalg

A = np.array([[1,3,5],[2,5,1],[2,3,8]])
>>> print(A)
array([[1, 3, 5],
      [2, 5, 1],
      [2, 3, 8]])
    
>>> b = np.array([10, 8, 3])

# solve Ax = b
>>> x = linalg.solve(A,b)
>>> print(x)
[-9.28  5.16  0.76]
```

##### Determinants
```python
>>> import numpy as np
>>> from scipy import linalg
>>> A = np.array([[1,2],[3,4]])
>>> print(A)
array([[1, 2],
      [3, 4]])
>>> linalg.det(A)
-2.0
```

##### Eigensystems
For matrix A, find v and $`\lambda`$ for
```math
A \vec{v} = \lambda \vec{v}
```

```python
>>> import numpy as np
>>> from scipy import linalg
>>> A = np.array([[1, 2], [3, 4]])
>>> print(A)
[[1 2]
 [2 3]]
>>> la, v = linalg.eig(A)

>>> print('Eigenvalue(s): ', la)
Eigenvalue(s):  [-0.23606798+0.j  4.23606798+0.j]
>>> print('Eigenvector(s): ', v)
Eigenvector(s):  [[-0.85065081 -0.52573111]
 [ 0.52573111 -0.85065081]]
```


#### Statistics

Scipy has a bunch of statistical distributions built-in with a bunch of methods.
For instance, consider the normal gaussian distribution

```python
from scipy.stats import norm

# probability at point
>>> norm.pdf(0.2)
0.3910426939754559

# probability less than that point
>>> norm.cdf(0.2)
0.579259709439103

# random sampling
>>> norm.rvs(size=5)
array([-0.52366173,  0.4220613 , -0.26614271, -0.16893076,  0.52102184])
```

You can also do custom distributions with *rv_continuous* and *rv_discrete*

### Homework
1. If you have taken Optics, you likely know about the Fresnel Integrals:
```math
S(z) = \int_{0}^{z} \sin(\pi t^2 / 2) dt
```

```math
C(z) = \int_{0}^{z} \cos(\pi t^2 / 2) dt
```

If you can solve these analytically, please let me know so I can kill you and steal the solution. Until then, we're stuck solving them numerically.

For a few values of z, compare the output of [scipy.special.fresnel](https://scipy.github.io/devdocs/reference/generated/scipy.special.fresnel.html#scipy.special.fresnel) to your own numerical integration using [scipy.integrate.quad](https://scipy.github.io/devdocs/reference/generated/scipy.integrate.quad.html#scipy.integrate.quad)

2. I hate bessel functions. They appear everywhere.
```math
x^2 \frac{d^2 y}{d x^2} + x \frac{dy}{dx} + (x^2 - \alpha^2)y = 0
```
Using order $`\alpha = 2`$, solve this for a few values of x using [scipy.special.jv](https://scipy.github.io/devdocs/reference/generated/scipy.special.jv.html#scipy.special.jv)

3. I have another differential equaitons that isn't baked into scipy.
```math
\frac{dy}{dx} = 4\sin(x) - 4xy + 3x^2(y^4-\sin(xy))
```
Could you please solve this using [scipy.integrate.solve_ivp](https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.solve_ivp.html#scipy.integrate.solve_ivp) for x = [-1, 1, 2, 5]

4. In quantum mechanics, observing a property of a particle collapses that particle into one of its eigenstates according to
```math
\hat{R} \vec{\psi} = R \vec{\psi}
```
for observable $`R`$ corresponding to operator $`\hat{R}`$

The *Rocheian* is a new observable I have just discovered. Its corresponding operator is
```math
\hat{R} = \begin{bmatrix}
1 & 1+i & 2i \\
1-i & 5 & -3 \\
-2i & -3 & 0
\end{bmatrix}
```
You measure R to be about 6.57. What eigenstate $`\vec{\psi}`$ did you collapse the particle into?