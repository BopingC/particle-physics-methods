import numpy as np
from time import process_time


# generate uniformly random data between -5 and 5
data = np.random.uniform(-5, 5, size=100000)

# use 100 evenly spaced bins from -5 to 5
bins = np.linspace(-5, 5, 100)

# use inefficient, trivial looping to check each bin in normal python
t1 = process_time()

bin_indices_py = []
for x in data:
    for i in range(len(bins)-1):
        if (x >= bins[i] and x <= bins[i+1]):
            bin_indices_py.append(i)
            break # kill for loop when you find right bin

t2 = process_time()
python_time = t2 - t1

# use numpy.digitize for same task
t1 = process_time()

bin_indices_np = np.digitize(data, bins)

t2 = process_time()
numpy_time = t2 - t1

# print results
print('Python time: ', python_time)
print('Numpy time: ', numpy_time)
print('Numpy speed improvement: ', round(python_time / numpy_time), 'X faster')

