# I used the pylorentz package, which can be installed with
# pip install pylorentz
# in the command line
import pylorentz

p1 = pylorentz.Momentum4.m_eta_phi_pt(125, 0.3, 2.2, 400)
p2 = pylorentz.Momentum4.e_eta_phi_p(400,  4.3, -0.7, 200)

two_particle_system = p1 + p2
print('Invariant Mass:', two_particle_system.m)
