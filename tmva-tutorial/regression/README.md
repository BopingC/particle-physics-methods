# Regression

Regression is very similar to classification. The significant difference is that instead of finding the output *class* of the event, you are trying to estimate a value. For instance, rather than trying to determine whether an event is an electron or a pion, you may want to estimate the energy of the electron based on other, available variables.

Here, we will show how to use a forest of BDTs for regression. If you compare [regression-training.py](regression-training.py) to its classification counterpart from the last lecture, you'll notice it is almost entirely identical, except for one or two suble changes, such as the addition of a *TargetVariable* (the regression output) rather than a class.

## Homework
Do the following:
- run [regression-training.py](regression-training.py)

This will:
- create the dataset in sinusoidal_1.root (shown in sinusoidal_1.png)
- train a regression algorithm to approximate *target* given the variable *v0*

Do the following:
- look at *training-output.root* in a TBrowser
- in there, *BDT* describes the approximation of the different points given *v0*, while *target* is the truth value
- in ROOT, type in *TMVA::TMVARegGui("training-output.root");* and play around with the GUI. Especially look at *4a*, *4c*, and *5*. What do these plots show?
- now go to [this website](http://tmva.sourceforge.net/old_site/optionRef.html#MVA::BDT) and see what values you can change in line 34 of *regression-training.py*. Can you make the regression better by messing with these? Try adding more trees, more depth, increasing nCuts, and changing BoostType

Finally, plot the following and skype me the results:
- histogram of (regression output / Truth) for the test points
- TProfile of (regression output / Truth) vs Truth for the test points
- plot target vs v0 as dots (tgraph) and on top of it do BDT vs v0 connecting the dots (tgraph)

![sinusoidal_1.png](./sinusoidal_1.png)