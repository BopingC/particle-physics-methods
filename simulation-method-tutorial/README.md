# Running simulations with Madgraph, Delphes, and Pythia

## Code used for the paper: Neural networks for boosted di-tau identification
Author for the paper: Nadav Tamir, Ilan Bessudo, Boping Chen, Hely Raiko, and Liron Barak

## Getting Started
- Run *setupSimulations.sh*
- This will install Madgraph, Delphes, and Pythia here and make them all talk to each other
- Do this **every time** you want to run simulations to initialize the software. It will only download them all the first time, but it's needed every time to initialize them.

### Making Pileup
run *createPileup.sh*

## Sample for the paper
The ttX and ttbar samples with analysis script can be found in [ttX](ttX) and [ttbar](ttbar)
